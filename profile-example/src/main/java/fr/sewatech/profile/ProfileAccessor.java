package fr.sewatech.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.stream.Stream;

@Component
public class ProfileAccessor {

    private static final Logger logger = LoggerFactory.getLogger(ProfileAccessor.class);
    private final ConfigurableEnvironment environment;
    @Value("${spring.profiles.active:}")
    private String activeProfilesFromProperties;

    public ProfileAccessor(ConfigurableEnvironment environment) {
        this.environment = environment;
    }

    @PostConstruct
    public void init() {
        logger.warn("Active profile(s) from property: " + activeProfilesFromProperties);
        logger.warn("Active profile(s) from environment: " + Arrays.toString(environment.getActiveProfiles()));
    }

    public String[] getActiveProfiles() {
        return environment.getActiveProfiles();
    }

    public boolean isProfileActive(String profile) {
        return Stream.of(environment.getActiveProfiles())
                .anyMatch(activeProfile -> activeProfile.equals(profile));
    }

    public void addActiveProfile(String profile) {
        environment.addActiveProfile(profile);
    }

}