package fr.sewatech.profile.annotations;

import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Profile(KnownProfiles.PROD)
@Component
public @interface ProdOnlyComponent {

    @AliasFor(annotation = Component.class, attribute = "value")
    String value() default "";
}