package fr.sewatech.profile.annotations;

public interface KnownProfiles {
    String DEV = "dev";
    String PROD = "prod";
}
