package fr.sewatech.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNameAware;

import javax.annotation.PostConstruct;

public class ProfileComponent implements BeanNameAware {

    private static final Logger logger = LoggerFactory.getLogger(ProfileComponent.class);

    private final String profile;
    private String beanName;

    public ProfileComponent(String profile) {
        this.profile = profile;
    }

    @PostConstruct
    public void init() {
        logger.warn("Bean: {}, profile: {}", this.beanName, this.profile);
    }

    @Override
    public void setBeanName(String name) {
        beanName = name;
    }

    public String getProfile() {
        return profile;
    }
}
