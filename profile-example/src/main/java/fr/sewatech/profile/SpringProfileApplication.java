package fr.sewatech.profile;

import fr.sewatech.profile.annotations.KnownProfiles;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.StandardEnvironment;

import javax.annotation.PostConstruct;
import java.util.stream.Stream;

@SpringBootApplication
public class SpringProfileApplication {

    private final ConfigurableEnvironment environment;

    public SpringProfileApplication
            (ConfigurableEnvironment environment) {
        this.environment = environment;
    }

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active", ProfilesConfiguration.SYSTEM_PROPERTY_ACTIVE);
        System.setProperty("spring.profiles.include", ProfilesConfiguration.SYSTEM_PROPERTY_INCLUDE);

        RunMode.from(args).main.run();
    }

    private static ConfigurableEnvironment buildEnvironment() {
        ConfigurableEnvironment environment = new StandardEnvironment();
        environment.setActiveProfiles(ProfilesConfiguration.ENV);
        return environment;
    }

    private static void mainSimple() {
        SpringApplication.run(SpringProfileApplication.class);
    }

    private static void mainAdvanced() {
        SpringApplication application = new SpringApplication(SpringProfileApplication.class);
        application.setEnvironment(buildEnvironment());
        application.setAdditionalProfiles(ProfilesConfiguration.ADDITIONAL, KnownProfiles.DEV);
        application.run();
    }

    private static void mainBuilder() {
        new SpringApplicationBuilder()
                .sources(SpringProfileApplication.class)
                .environment(buildEnvironment())
                .profiles(ProfilesConfiguration.ADDITIONAL, KnownProfiles.DEV) // => additionalProfiles
                .build()
                .run();
    }

    @PostConstruct
    public void init() {
        // ne sert à rien l'application context est déjà initialisé
        environment.addActiveProfile(ProfilesConfiguration.ENV_POST_CONSTRUCT);
    }

    private enum RunMode {
        SIMPLE(SpringProfileApplication::mainSimple),
        ADVANCED(SpringProfileApplication::mainAdvanced),
        BUILDER(SpringProfileApplication::mainBuilder);

        private final Runnable main;

        RunMode(Runnable main) {
            this.main = main;
        }

        static RunMode from(String[] args) {
            return Stream.of(args)
                    .findFirst()
                    .map(String::toUpperCase)
                    .map(RunMode::valueOf)
                    .orElse(SIMPLE);
        }
    }

}

