package fr.sewatech.profile;

import fr.sewatech.profile.annotations.DevOnlyComponent;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.annotation.Profile;

@DevOnlyComponent
@Profile("other-profile")
public class DevProfileComponent implements BeanNameAware {

    @Override
    public void setBeanName(String name) {
        System.out.println("==== Bean name: " + name);
    }
}
