package fr.sewatech.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ProfilesConfiguration {

    static final String DEFAULT = "default";

    static final String TEST = "test";
    static final String ENV = "environment";
    static final String ENV_POST_CONSTRUCT = "environment_post_construct";
    static final String ADDITIONAL = "additional";
    static final String SYSTEM_PROPERTY_ACTIVE = "system-property-active";
    static final String SYSTEM_PROPERTY_INCLUDE = "system-property-include";

    @Bean
    @Profile(SYSTEM_PROPERTY_ACTIVE)
    public ProfileComponent profile1() {
        return new ProfileComponent(SYSTEM_PROPERTY_ACTIVE);
    }

    @Bean
    @Profile(SYSTEM_PROPERTY_ACTIVE)
    public ProfileComponent profile2() {
        return new ProfileComponent(SYSTEM_PROPERTY_ACTIVE);
    }

    @Bean
    @Profile(DEFAULT)
    public ProfileComponent defaultProfile() {
        return new ProfileComponent(DEFAULT);
    }

    @Bean
    @Profile(TEST)
    public ProfileComponent test() {
        return new ProfileComponent(TEST);
    }
}
