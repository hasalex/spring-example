package fr.sewatech.profile;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(
        classes = SpringProfileApplication.class)
@ActiveProfiles(value = "test", resolver = EnhancedActiveProfileResolver.class)
class ProfileComponentTest {

    @Autowired
    ProfileComponent componentUnderTest;

    @Test
    void test_getProfile() {
        assertThat(componentUnderTest.getProfile()).contains("test");
    }
}