package fr.sewatech.profile;

import org.springframework.test.context.ActiveProfilesResolver;
import org.springframework.test.context.support.DefaultActiveProfilesResolver;

import java.util.stream.Stream;

public class EnhancedActiveProfileResolver implements ActiveProfilesResolver {

    public static final String PROPERTY_KEY = "spring.profiles.active";
    private final DefaultActiveProfilesResolver defaultActiveProfilesResolver = new DefaultActiveProfilesResolver();

    @Override
    public String[] resolve(Class<?> testClass) {
        return Stream
                .concat(
                        Stream.of(defaultActiveProfilesResolver.resolve(testClass)),
                        Stream.of(getPropertyProfiles())
                )
                .toArray(String[]::new);
    }

    private String[] getPropertyProfiles() {
        return System.getProperties().containsKey(PROPERTY_KEY)
                ? System.getProperty(PROPERTY_KEY).split("\\s*,\\s*")
                : new String[0];
    }
}