package fr.sewatech.profile;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import static org.assertj.core.api.Assertions.assertThat;

@CustomSpringBootTest(
        classes = SpringProfileApplication.class)
class ProfileComponentTestWithMetaAnnotation {

    @Autowired
    ProfileComponent componentUnderTest;
    @Autowired
    Environment environment;

    @Test
    void test_getProfile() {
        assertThat(componentUnderTest.getProfile()).contains("test");
        assertThat(environment.getActiveProfiles()).contains("test");
    }
}